//
//  DiaryDetailView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 20/08/21.
//

import SwiftUI

struct DiaryDetailView: View {
    @State var diary : Record
    @State var isDelete = false
    
    @ObservedObject var audioPlayer = AudioPlayer()
    @ObservedObject var audioRecorder: AudioRecorder
    
    @Environment(\.managedObjectContext) var context
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var diaryVM : DiaryViewModel
    
    var body: some View {
        ScrollView{
            VStack{
                Text(diaryVM.getEmo(mood: diary.mood ?? ""))
                    .frame(maxWidth: 80, minHeight: 80)
                    .font(.system(size: 40))
                    .background(diaryVM.getForegroundColor(mood: diary.mood ?? ""))
                    .cornerRadius(50)
                    
                Text((diary.desc == "" ? "No Description" : diary.desc) ?? "No Description")
                        .font(.system(size: 14))
                        .padding()
                
                Spacer(minLength: 10)
                
                if audioPlayer.isPlaying == false {
                    Button("Listen") {
                        self.audioPlayer.startPlayback(audio: URL(string: self.diary.fileURL ?? "")!)
                    }
                    .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                    .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                    .cornerRadius(20)
                    .buttonStyle(BorderedButtonStyle())
                } else {
                    Button("Stop") {
                        self.audioPlayer.stopPlayback()
                    }
                    .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                    .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                    .cornerRadius(20)
                    .buttonStyle(BorderedButtonStyle())
                }
                
                Spacer(minLength: 20)
                
                NavigationLink(
                    destination: EditDiaryView(diary: $diary),
                    label: {
                        Text("Edit")
                    })
                    .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                    .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                    .cornerRadius(10)
                    .buttonStyle(BorderedButtonStyle())
                
                Button("Delete") {
                    isDelete = true
                }
                .foregroundColor(Color(#colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1)))
                .background(Color(#colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)))
                .cornerRadius(10)
                .buttonStyle(BorderedButtonStyle())
                
            }
            .onAppear(perform: {
                diaryVM.fetchDiary()
            })
        }
        .alert(isPresented: $isDelete, content: {
            Alert(title: Text("Delete Diary"), message: Text("Are you sure you want to delete this diary?"), primaryButton: .default(Text("Okay"), action: {
                delete(urlToDelete: URL(string: diary.fileURL ?? "")!)
            }), secondaryButton: .destructive(Text("Cancel")))
        })
        .navigationTitle(diary.dateAdded?.toString(dateFormat: "dd MMM") ?? "")
    }
    
    func delete(urlToDelete : URL) {
        isDelete = false
        audioRecorder.deleteRecording(urlsToDelete: [urlToDelete])
        print(diaryVM.result)
        print(diary)
        if diaryVM.deleteADiary(diary: diary) { self.presentationMode.wrappedValue.dismiss() }
        
    }
}

//struct DiaryDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DiaryDetailView(diary: <#Record#>, record: Recording(fileURL: URL(fileURLWithPath: ""), createdAt: Date()))
//    }
//}
