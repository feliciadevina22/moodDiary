//
//  DiaryRowView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 20/08/21.
//

import SwiftUI

struct DiaryRowView: View {
    var diary: Record
    var diaryVM : DiaryViewModel = DiaryViewModel()
    @ObservedObject var audioPlayer = AudioPlayer()
    
    var body: some View {
        HStack {
            VStack(alignment: .leading){
                Text(diary.dateAdded?.toString(dateFormat: "dd MMM") ?? "")
                    .font(.system(size: 24))
                Text(diary.dateAdded?.toString(dateFormat: "E") ?? "")
                    .font(.subheadline)
            }
            Spacer()
            Text(diaryVM.getEmo(mood: diary.mood ?? ""))
                .font(.system(size: 40))
                .padding(.vertical, 2)
                .padding(.horizontal, 4)
                .background(diaryVM.getForegroundColor(mood: diary.mood ?? ""))
                .cornerRadius(50)
        }
        .padding()
        .background(diaryVM.getBackgroundColor(mood: diary.mood ?? ""))
        .cornerRadius(10)
    }
}

//struct DiaryRowView_Previews: PreviewProvider {
//    static var previews: some View {
//        DiaryRowView(diary: <#Record#>, audio: Recording(fileURL: URL(fileURLWithPath: ""), createdAt: Date()))
//    }
//}
