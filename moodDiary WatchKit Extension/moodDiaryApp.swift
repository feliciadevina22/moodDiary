//
//  moodDiaryApp.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 16/08/21.
//

import SwiftUI

@main
struct moodDiaryApp: App {
    @Environment(\.managedObjectContext) public var context
    let container = PersistenceController.shared.container
    @SceneBuilder var body: some Scene {
        WindowGroup {
            NavigationView {
                ContentView()
            }
            .environment(\.managedObjectContext, container.viewContext)
        }

        WKNotificationScene(controller: NotificationController.self, category: "myCategory")
    }
}
