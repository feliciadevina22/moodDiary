//
//  NewDiaryView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 16/08/21.
//

import SwiftUI
import AVFoundation
import WatchKit

struct NewDiaryView: View {
    @ObservedObject var audioRecorder: AudioRecorder
    @ObservedObject var audioPlayer = AudioPlayer()
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var url = URL(string: "")
    @State var startRecord = false
    @State var isSaved = false
    
    var body: some View {
        VStack{
            if audioRecorder.recording == false && startRecord == false {
                Button(action: {
                    startRecord = true
                    print(self.audioRecorder.startRecording())
                }) {
                    Image(systemName: "mic.fill")
                        .resizable()
                        .padding(30)
                        .frame(width: 90, height: 110)
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                        .background(Color(#colorLiteral(red: 0.6745098039, green: 0.7450980392, blue: 0.8745098039, alpha: 1)))
                        .clipShape(Circle())
                }
                .buttonStyle(PlainButtonStyle())
                
            } else if audioRecorder.recording == true && startRecord == true {
                Button(action: {
                    self.audioRecorder.stopRecording()
                }) {
                    Image(systemName: "dot.square.fill")
                        .resizable()
                        .padding(30)
                        .frame(width: 90, height:90)
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                        .background(Color(#colorLiteral(red: 0.6745098039, green: 0.7450980392, blue: 0.8745098039, alpha: 1)))
                        .clipShape(Circle())
                    
                }
                .buttonStyle(PlainButtonStyle())
            } else {
                Spacer()
                
                if(audioPlayer.isPlaying == false){
                    Button(action: {
                        self.audioPlayer.startPlayback(audio: audioRecorder.getLastUrl())
                    }) {
                        Image(systemName: "play.fill")
                            .frame(width: 90, height:90)
                            .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                            .background(Color(#colorLiteral(red: 0.6745098039, green: 0.7450980392, blue: 0.8745098039, alpha: 1)))
                            .clipShape(Circle())
                    }
                    .buttonStyle(PlainButtonStyle())
                }else{
                    Button(action: {
                        self.audioPlayer.stopPlayback()
                    }) {
                        Image(systemName: "square.fill")
                            .resizable()
                            .padding(30)
                            .frame(width: 90, height:90)
                            .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                            .background(Color(#colorLiteral(red: 0.6745098039, green: 0.7450980392, blue: 0.8745098039, alpha: 1)))
                            .clipShape(Circle())
                    }
                    .buttonStyle(PlainButtonStyle())
                }
                
                Spacer()
                Spacer()
                
                HStack{
                    Button(action: {
                        if(audioPlayer.isPlaying == true){
                            self.audioPlayer.stopPlayback()
                        }
                        self.audioRecorder.deleteRecording(urlsToDelete: [audioRecorder.getLastUrl()])
                        print(self.audioRecorder.startRecording())
                    }, label: {
                        Text("Redo")
                            .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                    })
                    .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                    .cornerRadius(20)
                    
                    NavigationLink(
                        destination: MoodChooseView(isSave: $isSaved, audioRecorder: audioRecorder),
                        label: {
                            Text("Next")
                                .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                        })
                        .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                        .cornerRadius(20)
                }
            }
        }
        .padding()
        .navigationTitle("Add Diary")
        .onAppear(perform: {
            if $isSaved.wrappedValue {
                self.presentationMode.wrappedValue.dismiss()
            }
        })
        
        
    }
    
}

struct NewDiaryView_Previews: PreviewProvider {
    static var previews: some View {
        NewDiaryView(audioRecorder: AudioRecorder())
    }
}
