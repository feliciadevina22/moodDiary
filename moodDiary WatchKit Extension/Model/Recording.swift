//
//  Recording.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 18/08/21.
//

import Foundation
import AVFoundation

//public struct Recording {
//    var id = UUID()
//    var date : Date?
//    var audio : AVAudioEngine?
//    var description : String?
//    var emotion : [String? : String?]
//}

struct Recording {
    let fileURL: URL
    let createdAt: Date
}

public var allEmotion : [String:String] = ["Happy" : "😆", "In Love" : "🥰", "Mad" : "🤬", "Sad" : "😢", "Like a Poo" : "💩", "Clown" : "🤡", "Disgust" : "🤢", "Dead" : "😵", "Dizzy" : "😵‍💫", "This is Life" : "🥲", "Curious":"👀", "Mind Blown":"🤯", "Crazy":"🤪", "Oh my":"😳", "I'm touched":"🥺"]
