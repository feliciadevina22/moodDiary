//
//  EditDiaryView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 26/08/21.
//

import SwiftUI

struct EditDiaryView: View {
    @Binding var diary : Record
    @State var isSave : Bool = false
    @State var text : String = ""
    
    var diaryVm : DiaryViewModel = DiaryViewModel()
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        VStack {
            TextField("Description", text: $text)
                .onAppear(perform: {
                    $text.wrappedValue = diary.desc ?? ""
                })
            
            Button(action: {
                $isSave.wrappedValue = diaryVm.editDiary(diary: diary, description: $text.wrappedValue)
            }, label: {
                Text("Save")
            })
            .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)).opacity(text.isEmpty ? 0.5 : 1))
            .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)).opacity(text.isEmpty ? 0.2 : 1))
            .cornerRadius(20)
            .disabled(text.isEmpty)
        }
        .onChange(of: isSave, perform: { value in
            self.presentationMode.wrappedValue.dismiss()
        })
    }
}

//struct EditDiaryView_Previews: PreviewProvider {
//    static var previews: some View {
//        EditDiaryView()
//    }
//}
