//
//  ViewDiaryView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 20/08/21.
//

import SwiftUI

struct ViewDiaryView: View {
    @ObservedObject var audioRecorder: AudioRecorder
    @StateObject var DiaryVM : DiaryViewModel = DiaryViewModel()
    
    var body: some View {
        List{
            ForEach(DiaryVM.result){ i in
                NavigationLink(
                    destination: DiaryDetailView(diary: i, audioRecorder: audioRecorder, diaryVM: DiaryVM),
                    label: {
                        DiaryRowView(diary: i)
                    })
            }.onDelete(perform: delete)
            .onAppear(perform: {
                print(DiaryVM.result)
                self.audioRecorder.fetchRecordings()
            })
        }
        .overlay(DiaryVM.result.isEmpty ? Text("No Diary Yet").foregroundColor(Color(#colorLiteral(red: 0.6745098039, green: 0.7450980392, blue: 0.8745098039, alpha: 1))) : Text(""))
        .padding(.top, 10)
        
//        List {
//            ForEach(audioRecorder.recordings, id: \.createdAt) { recording in
//                NavigationLink(
//                    destination: Text(recording.fileURL.absoluteString),
//                    label: {
//                        Text(recording.fileURL.absoluteString)
//                    })
//            }
//            .onDelete(perform: delete)
//        }
//        .onAppear(perform: {
//            self.audioRecorder.fetchRecordings()
//        })
//        .padding(.top, 10)
    }
    
    func delete(at offsets: IndexSet) {
        var urlsToDelete = [URL]()
        
        for index in offsets {
            urlsToDelete.append(audioRecorder.recordings[index].fileURL)
        }
        audioRecorder.deleteRecording(urlsToDelete: urlsToDelete)
        DiaryVM.deleteDiary(at: offsets)
    }
}

struct ViewDiaryView_Previews: PreviewProvider {
    static var previews: some View {
        ViewDiaryView(audioRecorder: AudioRecorder())
    }
}
