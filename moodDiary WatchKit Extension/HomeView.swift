//
//  HomeView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 16/08/21.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        VStack{
            Spacer()
            
            NavigationLink(
                destination: NewDiaryView(audioRecorder: AudioRecorder()),
                label: {
                    Text("Add Diary")
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                })
                .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                .cornerRadius(20)
            
            
            NavigationLink(
                destination: ViewDiaryView(audioRecorder: AudioRecorder()),
                label: {
                    Text("View Diary")
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                })
                .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)))
                .cornerRadius(20)
            
            
            Spacer()
            
            Text("N O S T A L G I C")
                .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                .font(.system(size: 10))
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
