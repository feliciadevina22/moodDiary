
import Foundation
import SwiftUI
import CoreData

class DiaryViewModel : ObservableObject {
    
    @Published var result : [Record] = []
//    @Environment(\.managedObjectContext) var context
   
    let container : NSPersistentContainer
    
//    @FetchRequest(entity: Record.entity() , sortDescriptors: [NSSortDescriptor(keyPath: \Record.dateAdded, ascending: false)], animation: .easeIn) var results : FetchedResults<Record>
    
    init(){
        container = NSPersistentContainer(name: "Diary")
        container.loadPersistentStores { (description, error) in
            if let error = error {
                print("ERROR LOADING CORE DATA. \(error)")
            }
        }
        fetchDiary()
    }
    
    func fetchDiary() {
        let request = NSFetchRequest<Record>(entityName: "Record")
        
        do{
            result = try container.viewContext.fetch(request)
        }catch{
            print("Error Loading. \(error)")
        }
    }
    
    
    func addDiary(url : String, date : Date, mood : String, description : String, isDesc: Bool) -> Bool{
        
        let diary = Record(context: container.viewContext)
        
        diary.fileURL = url
        diary.dateAdded = date
        diary.mood = mood
        
        if isDesc {
            diary.desc = description
        }else{
            diary.desc = ""
        }
        
        print(diary)
        
        do{
            try container.viewContext.save()
            fetchDiary()
            return true
        }catch{
            print(error.localizedDescription)
            return false
        }
    }
    
    func deleteDiary(at offsets: IndexSet) {
        guard let index = offsets.first else { return }
        let entity = result[index]
        container.viewContext.delete(entity)

        do{
            try container.viewContext.save()
        }catch{
            error.localizedDescription
        }
        fetchDiary()
    }
    
    func deleteADiary(diary : Record) -> Bool{
        container.viewContext.delete(diary)

        do{
            try container.viewContext.save()
            fetchDiary()
            return true
        }catch{
            error.localizedDescription
            fetchDiary()
            return false
        }
    }
    
    func editDiary(diary: Record, description : String) -> Bool{
        let diary = diary
        
        diary.desc = description
        
        print(diary)
        
        do{
            try container.viewContext.save()
            fetchDiary()
            return true
        }catch{
            print(error.localizedDescription)
            return false
        }
    }
    
    func getEmo(mood : String) -> String {
        switch mood {
        case "Happy":
            return "😆"
        case "In Love":
            return "🥰"
        case "Mad" :
            return "🤬"
        case "Sad" :
            return "😢"
        case "Like a Poo" :
            return "💩"
        case "Clown" :
            return "🤡"
        case "Disgust" :
            return "🤢"
        case "Dead" :
            return "😵"
        case "Dizzy" :
            return "😵‍💫"
        case "This is Life" :
            return "🥲"
        case "Curious":
            return "👀"
        case "Mind Blown":
            return "🤯"
        case "Crazy":
            return "🤪"
        case "Oh my":
            return "😳"
        case "I'm touched":
            return "🥺"
        default:
            return ""
        }
    }
    
    func getForegroundColor(mood : String) -> Color {
        switch mood {
        case "Happy":
            return Color(#colorLiteral(red: 0.9764705896, green: 0.850980401, blue: 0.5490196347, alpha: 1))
        case "In Love":
            return Color(#colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1))
        case "Mad" :
            return Color(#colorLiteral(red: 1, green: 0.667275846, blue: 0.7132451534, alpha: 1))
        case "Sad" :
            return Color(#colorLiteral(red: 0.4745098054, green: 0.8392156959, blue: 0.9764705896, alpha: 1))
        case "Like a Poo" :
            return Color(#colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1))
        case "Clown" :
            return Color(#colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 1))
        case "Disgust" :
            return Color(#colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1))
        case "Dead" :
            return Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1))
        case "Dizzy" :
            return Color(#colorLiteral(red: 0.9567827582, green: 0.9509969354, blue: 0.6800811887, alpha: 1))
        case "This is Life" :
            return Color(#colorLiteral(red: 0.8446714282, green: 0.7825533748, blue: 0.9696711898, alpha: 1))
        case "Curious":
            return Color(#colorLiteral(red: 0.7211534381, green: 0.9626424909, blue: 0.8997992277, alpha: 1))
        case "Mind Blown":
            return Color(#colorLiteral(red: 0.7930827737, green: 0.8366232514, blue: 0.9670516849, alpha: 1))
        case "Crazy":
            return Color(#colorLiteral(red: 0.9089737535, green: 0.8135153651, blue: 0.9445011616, alpha: 1))
        case "Oh my":
            return Color(#colorLiteral(red: 0.9612382054, green: 0.9298394322, blue: 0.8565608859, alpha: 1))
        case "I'm touched":
            return Color(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        default:
            return Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        }
    }
    
    func getBackgroundColor(mood : String) -> Color {
        switch mood {
        case "Happy":
            return Color(#colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        case "In Love":
            return Color(#colorLiteral(red: 0.9295812249, green: 0.0872188732, blue: 0.4558005929, alpha: 1))
        case "Mad" :
            return Color(#colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
        case "Sad" :
            return Color(#colorLiteral(red: 0.1411764771, green: 0.3960784376, blue: 0.5647059083, alpha: 1))
        case "Like a Poo" :
            return Color(#colorLiteral(red: 0.3098039329, green: 0.2039215714, blue: 0.03921568766, alpha: 1))
        case "Clown" :
            return Color(#colorLiteral(red: 1, green: 0.5213962793, blue: 0.1528261304, alpha: 1))
        case "Disgust" :
            return Color(#colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1))
        case "Dead" :
            return Color(#colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1))
        case "Dizzy" :
            return Color(#colorLiteral(red: 0.8321695924, green: 0.985483706, blue: 0.4733308554, alpha: 1))
        case "This is Life" :
            return Color(#colorLiteral(red: 0.5818830132, green: 0.2156915367, blue: 1, alpha: 1))
        case "Curious":
            return Color(#colorLiteral(red: 0, green: 0.5690457821, blue: 0.5746168494, alpha: 1))
        case "Mind Blown":
            return Color(#colorLiteral(red: 0.3647058904, green: 0.06666667014, blue: 0.9686274529, alpha: 1))
        case "Crazy":
            return Color(#colorLiteral(red: 1, green: 0.3599659204, blue: 0.8322571516, alpha: 1))
        case "Oh my":
            return Color(#colorLiteral(red: 1, green: 0.9179520011, blue: 0.3680375814, alpha: 1))
        case "I'm touched":
            return Color(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1))
        default:
            return Color(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        }
    }
    
}
