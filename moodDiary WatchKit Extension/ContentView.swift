//
//  ContentView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 16/08/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        HomeView()
            .navigationTitle("Nostalgic")
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
