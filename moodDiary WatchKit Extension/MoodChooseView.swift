//
//  MoodChooseView.swift
//  moodDiary WatchKit Extension
//
//  Created by Felicia Devina on 24/08/21.
//

import SwiftUI

struct MoodChooseView: View {
    @State var data = ""
    @State var isDesc = false
    @State var desc = ""
    @Binding var isSave : Bool
    
    @ObservedObject var audioRecorder: AudioRecorder
//    @ObservedObject var vm = PersistenceController
    var diaryVM = DiaryViewModel()
    
    @Environment(\.managedObjectContext) var context
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    var body: some View {
        ScrollView {
            VStack(alignment:.center){
                Text("How do you feel now?")
                    .font(.system(size: 14))
                    .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                
                Spacer(minLength: 5)
                
                ScrollView(.horizontal) {
                    HStack(spacing: 0){
                        ForEach(allEmotion.sorted(by: <), id: \.key) { key, value in
                            VStack(spacing: 10){
                                Text(value)
                                    .frame(width: 60, height: 60)
                                    .font(.system(size: 30))
                                    .background(diaryVM.getForegroundColor(mood: key).opacity(data == key ? 1 : 0))
                                    .cornerRadius(50)
                                Text(key)
                                    .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                                    .font(.system(size: 12))
                            }
                            .frame(minWidth:60, minHeight: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                            .onTapGesture {
                                data = key
                            }
                        }
                    }
                }
                Spacer(minLength: 10)
                
                Toggle(isOn: $isDesc) {
                    Text("Description")
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                        .font(.system(size: 14))
                }

                Spacer(minLength: 10)
                
                if($isDesc.wrappedValue){
                    TextField("Description", text: $desc)
                        .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)))
                        .font(.system(size: 14))
                    Spacer(minLength: 10)
                }
                
                Button(action: {
                    $isSave.wrappedValue = diaryVM.addDiary(url: audioRecorder.getLastUrl().absoluteString, date: getCreationDate(for: audioRecorder.getLastUrl()), mood: data, description: $desc.wrappedValue, isDesc: $isDesc.wrappedValue)
                }, label: {
                    Text("Save")
                })
                .foregroundColor(Color(#colorLiteral(red: 0.4549019608, green: 0.5647058824, blue: 0.768627451, alpha: 1)).opacity(checkData() && checkDescription() ? 1 : 0.5))
                .background(Color(#colorLiteral(red: 0.7864660025, green: 0.8713989854, blue: 1, alpha: 1)).opacity(checkData() && checkDescription()  ? 1 : 0.2))
                .cornerRadius(20)
                .disabled(!(checkData() && checkDescription()))
                
            }
            .onChange(of: isSave, perform: { value in
                self.presentationMode.wrappedValue.dismiss()
            })
            .padding()
            .navigationTitle("Add Diary")
        }
    }
    
    func checkData() -> Bool {
        if data.isEmpty {
            return false
        }
        return true
    }
    
    func checkDescription() -> Bool {
        if $isDesc.wrappedValue {
            if $desc.wrappedValue.isEmpty {
                return false
            }
            return true
        }
        return true
    }
    
    func delete(at offsets: IndexSet) {
        var urlsToDelete = [URL]()
        
        for index in offsets {
            urlsToDelete.append(audioRecorder.recordings[index].fileURL)
        }
        audioRecorder.deleteRecording(urlsToDelete: urlsToDelete)
        diaryVM.deleteDiary(at: offsets)

        do{
            try context.save()
        }catch{
            error.localizedDescription
        }
    }
}

struct MoodChooseView_Previews: PreviewProvider {
    static var previews: some View {
        MoodChooseView(isSave: .constant(false), audioRecorder: AudioRecorder())
    }
}
